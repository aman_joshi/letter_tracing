//
//  Extension + UIImage.swift
//  LiveCodingRound
//
//  Created by Aman Joshi on 17/03/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import UIKit

extension UIImageView {
    
    func getPixelColorAt(point:CGPoint) -> UIColor{
        
        let pixel = UnsafeMutablePointer<CUnsignedChar>.allocate(capacity: 4)
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        
        context!.translateBy(x: -point.x, y: -point.y)
        layer.render(in: context!)
        let color:UIColor = UIColor(red: CGFloat(pixel[0])/255.0,
                                    green: CGFloat(pixel[1])/255.0,
                                    blue: CGFloat(pixel[2])/255.0,
                                    alpha: CGFloat(pixel[3])/255.0)
        
        pixel.deallocate()
        return color
    }
    
    func isValidPath(color: UIColor?) -> Bool {
        guard let color = color else { return false }
        let compareColor = UIColor(displayP3Red: 207/255, green: 174/255, blue: 137/255, alpha: 1)
        
        let customCompareColor = CustomColor()
        let customTargetColor = CustomColor()
        
        compareColor.getRed(&customCompareColor.red, green: &customCompareColor.green, blue: &customCompareColor.blue, alpha: &customCompareColor.alpha)
        color.getRed(&customTargetColor.red, green: &customTargetColor.green, blue: &customTargetColor.blue, alpha: &customTargetColor.alpha)
        
        
        print(customTargetColor.red)
        print(customCompareColor.red)
        
        if customTargetColor < customCompareColor  {
            return true
        }
        return false
    }
}

class CustomColor:Comparable {
    
    static func < (lhs: CustomColor, rhs: CustomColor) -> Bool {
        return lhs.red < rhs.red && lhs.blue < rhs.blue && lhs.green < rhs.green
    }
    
    static func == (lhs: CustomColor, rhs: CustomColor) -> Bool {
        return lhs.red == rhs.red && lhs.blue == rhs.blue && lhs.green == rhs.green
    }
    
    var red:CGFloat   = 0
    var green:CGFloat = 0
    var blue:CGFloat  = 0
    var alpha:CGFloat = 0
}
