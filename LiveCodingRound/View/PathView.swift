//
//  PathView.swift
//  LiveCodingRound
//
//  Created by Aman Joshi on 17/03/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import UIKit

class PathView: UIView {
    
    override init(frame: CGRect) {
           super.init(frame: frame)
           self.backgroundColor = .green
       }
       
       required init?(coder: NSCoder) {
           super.init(coder: coder)
       }
    
}
