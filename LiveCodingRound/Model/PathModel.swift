//
//  PathModel.swift
//  LiveCodingRound
//
//  Created by Aman Joshi on 17/03/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import UIKit

struct PathModel {
    let x:CGFloat
    let y:CGFloat
}
