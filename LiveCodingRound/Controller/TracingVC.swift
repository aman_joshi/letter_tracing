//
//  TracingVC.swift
//  LiveCodingRound
//
//  Created by Aman Joshi on 16/03/21.
//  Copyright © 2021 Aman Joshi. All rights reserved.
//

import UIKit

class TracingVC: UIViewController {
    
    private let findColor = UIColor.white
    
    lazy var imageView:UIImageView = {
       let iv = UIImageView(image: UIImage(named: "Z-Alphabet"))
        iv.clipsToBounds = true
        iv.contentMode = .scaleAspectFit
        iv.isUserInteractionEnabled = true
        return iv
    }()
    
    lazy var cursorView:CursorView = {
        let view = CursorView()
        return view
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
        self.view.addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: view.frame.width * 0.8).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: view.frame.height * 0.9).isActive = true
        self.imageView.addSubview(cursorView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        cursorView.frame = CGRect(x: imageView.frame.width * 0.32, y: imageView.bounds.origin.y + 15, width: 30, height: 30)
        cursorView.layer.cornerRadius = 15.0
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Touches Began")
        if let touch = event?.allTouches?.first {
            if (touch.view?.isKind(of: CursorView.self))! {
                let location = touch.location(in: imageView)
                let pixelColor = self.imageView.getPixelColorAt(point: location)
                if self.imageView.isValidPath(color: pixelColor) {
                    self.createPath(at: location)
                }
            }
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = event?.allTouches?.first {
            if (touch.view?.isKind(of: CursorView.self))! {
                let location = touch.location(in: imageView)
                let pixelColor = self.imageView.getPixelColorAt(point: location)
                if self.imageView.isValidPath(color: pixelColor) {
                    self.createPath(at: location)
                }else{
                    print("White")
                }
            }
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        print("Touch Cancelled")
    }
    
    func createPath(at location:CGPoint) {
        let origin = CGPoint(x: self.cursorView.frame.origin.x - 10, y: self.cursorView.frame.origin.y)
        let path = PathView(frame: CGRect(origin: origin, size: CGSize(width: 30, height: 30)))
        self.cursorView.center = location
        self.imageView.addSubview(path)
        path.layer.cornerRadius = 15
    }
}


